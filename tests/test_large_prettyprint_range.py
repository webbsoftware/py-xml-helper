import datetime
import os

import pytest

from src.xml_streaming_helper import xmlutillib as UT

LARGE_XML_FILENAME = r'C:\Peregrine\download\WADS\DATA\Jan2021.xml'
LARGE_XML_OUTPUT   = r'C:\Peregrine\download\WADS\DATA\Jan2021.xml.first10.xml'
LARGE_XML_OUTPUT   = r'C:\Peregrine\download\WADS\DATA\Jan2021.xml.15000.xml'

LARGE_XML_FILENAME = r'C:\Peregrine\download\WADS\0129\Feb2021.xml'
LARGE_XML_OUTPUT   = r'C:\Peregrine\download\WADS\0129\Feb2021.xml.100.xml'

def remove_file_if_exists(filename:str):
    if os.path.exists(filename):
        os.remove(filename)


def test_parge_prettyprint_range():
    remove_file_if_exists(LARGE_XML_OUTPUT)
    start_dt = datetime.datetime.now()

    #UT.pretty_print_xml_range(LARGE_XML_FILENAME, LARGE_XML_OUTPUT, range(1,5001))
    #UT.pretty_print_xml_range(LARGE_XML_FILENAME, LARGE_XML_OUTPUT, range(5001,10001))
    UT.pretty_print_xml_range(LARGE_XML_FILENAME, LARGE_XML_OUTPUT, range(1,101))

    end_dt = datetime.datetime.now()
    print("")
    print("Time: ", (end_dt - start_dt))
