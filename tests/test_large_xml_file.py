import datetime
import pytest

from src.xml_streaming_helper import xml_processor

LARGE_XML_FILENAME = r'C:\Peregrine\download\WADS\DATA\Jan2021.xml'

def test_processor_parse_passes():
    pp = xml_processor.LargeXMLProcessor()

    start_dt = datetime.datetime.now()
    item_count = pp.load_file(LARGE_XML_FILENAME)
    end_dt = datetime.datetime.now()

    print("Items: ", item_count)
    print("Time: ", (end_dt - start_dt))
    assert item_count > 10000
