import pytest

from lxml import etree as ET

from src.xml_streaming_helper import xml_handler
from src.xml_streaming_helper import xml_processor

SAMPLE01_FILENAME = r'data\sample01.xml'


@pytest.fixture()
def lpp():
    return xml_processor.LargeXMLProcessor()


def test_processor_init_passes():
    assert xml_processor.LargeXMLProcessor()


def test_processor_parse_passes():
    pp = xml_processor.LargeXMLProcessor()
    item_count = pp.load_file(SAMPLE01_FILENAME)
    assert 2 == item_count


def test_handler_init_passes():
    assert xml_handler.LargeXMLContentHandler()


def sample_handler(master_tag:str, tree:ET.Element):
    if len(tree):
        print(master_tag, tree.tag)


def test_add_handler_passes(lpp):
    lpp.add_post_handler(sample_handler)
    assert True
    print("")
    item_count = lpp.load_file(SAMPLE01_FILENAME)
    assert 2 == item_count


def test_add_output_passes(lpp):
    with open(r'output/test_output.xml', 'w') as fh:
        lpp.add_output(fh)

        print("")
        item_count = lpp.load_file(SAMPLE01_FILENAME)
        assert 2 == item_count


def test_add_rangefilter_one_item_passes(lpp):
    lpp.add_rangefilter(range(1,2))
    print("")
    item_count = lpp.load_file(SAMPLE01_FILENAME)
    assert 1 == item_count


def test_add_rangefilter_zero_item_passes(lpp):
    lpp.add_rangefilter(range(5,6))
    print("")
    item_count = lpp.load_file(SAMPLE01_FILENAME)
    assert 0 == item_count


def test_count_dict_is_not_empty(lpp):
    print("")
    item_count = lpp.load_file(SAMPLE01_FILENAME)
    summary_dict = lpp.get_total_dict()
