import os
import sys
import pytest

from src.xml_streaming_helper import xmlutillib as UT

SAMPLE01_FILENAME = r'data\sample01.xml'
SAMPLE02_FILENAME = r'data\sample02.xml'


def remove_file_if_exists(filename:str):
    if os.path.exists(filename):
        os.remove(filename)


def test_lib_prettyprint_all_passes():
    new_filename = r'output/lib_prettyprint-all.xml'
    remove_file_if_exists(new_filename)

    UT.pretty_print_xml(SAMPLE01_FILENAME, new_filename)

    assert os.path.exists(new_filename)


def test_lib_prettyprint_range_passes():
    new_filename = r'output/lib_prettyprint-range.xml'
    remove_file_if_exists(new_filename)

    UT.pretty_print_xml_range(SAMPLE01_FILENAME, new_filename, range(2,3))

    assert os.path.exists(new_filename)

def test_lib_splitfile_passes():
    UT.split_file(SAMPLE01_FILENAME, 1)

def test_lib_countitems_passes():
    UT.count_items(SAMPLE01_FILENAME, sys.stdout)

def test_lib_summarizeitems_passes():
    UT.summarize_items(SAMPLE01_FILENAME, sys.stdout)

def test_mergexml_passes():
    new_filename = r'output/lib_merged.xml'
    UT.merge_files(
        [SAMPLE01_FILENAME, SAMPLE02_FILENAME],
        new_filename)
