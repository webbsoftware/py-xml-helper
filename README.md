# XML Streaming Helper

Encapsulates and facilitates processing of XML file (usually large) using a streaming API (SAX) rather than DOM API.

Individual top-level (underneath root) are processed and exposed using the LXML/ElementTree library.
