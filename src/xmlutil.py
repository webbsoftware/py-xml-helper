import argparse
import os
import sys

import xml_streaming_helper.xmlutillib as UT


CLI_KEY_CMD         = "cmd"
CLI_KEY_COUNT       = "count"
CLI_KEY_SUMMARIZE   = "summarize"
CLI_KEY_EXTRACT     = 'extract'
CLI_KEY_MERGE       = 'merge'

CLI_KEY_IFILE       = "ifile"
CLI_KEY_IFILES      = "ifiles"
CLI_KEY_OFILE       = "ofile"
CLI_KEY_START       = "startno"
CLI_KEY_END         = "endno"

def run_parser(args):
    default_config = os.path.splitext(os.path.split(sys.argv[0])[1])[0] + ".ctl"
    spacer = "   "
    this_parser = argparse.ArgumentParser(		formatter_class=argparse.RawDescriptionHelpFormatter,
                                                  description='XMLUTIL - XML Utilities',
                                                  epilog= ""
                                        )
    subparsers = this_parser.add_subparsers(help='commands', dest=CLI_KEY_CMD)
    subparse_dict = dict()

    # global options
    pass

    # sub-parser: COUNT
    subparse_dict[CLI_KEY_COUNT]    = subparsers.add_parser(CLI_KEY_COUNT,        help='Count items inside of outermost parent')
    if True:
        subparse_dict[CLI_KEY_COUNT].add_argument(CLI_KEY_IFILE,             action='store',  help='Input filename.')

    # sub-parser: SUMMARIZE
    subparse_dict[CLI_KEY_SUMMARIZE]    = subparsers.add_parser(CLI_KEY_SUMMARIZE,        help='Provide type and count of outermost parents')
    if True:
        subparse_dict[CLI_KEY_SUMMARIZE].add_argument(CLI_KEY_IFILE,             action='store',  help='Input filename.')

    # sub-parser: EXTRACT
    subparse_dict[CLI_KEY_EXTRACT]   = subparsers.add_parser(CLI_KEY_EXTRACT,      help='Extract range of top-level items')
    if True:
        subparse_dict[CLI_KEY_EXTRACT].add_argument(CLI_KEY_IFILE,             action='store',  help='Input filename')
        subparse_dict[CLI_KEY_EXTRACT].add_argument(CLI_KEY_OFILE,             action='store',  help='Output filename')
        subparse_dict[CLI_KEY_EXTRACT].add_argument(CLI_KEY_START,             action='store',  help='Starting item number')
        subparse_dict[CLI_KEY_EXTRACT].add_argument(CLI_KEY_END,             action='store',  help='Ending item number')

    # sub-parser: MERGE
    subparse_dict[CLI_KEY_MERGE]   = subparsers.add_parser(CLI_KEY_MERGE,      help='Merge XML files into 1 file')
    if True:
        subparse_dict[CLI_KEY_MERGE].add_argument(CLI_KEY_IFILES,            action='store',  help='Input filenames', nargs='+')
        subparse_dict[CLI_KEY_MERGE].add_argument(CLI_KEY_OFILE,             action='store',  help='Output filename')

    this_namespace	= this_parser.parse_args(args)
    this_argdict	= this_namespace.__dict__

    return this_namespace, this_argdict

def is_valid_file(filename:str) -> bool:
    return os.path.exists(filename)


def main():
    this_namespace, this_argdict = run_parser(sys.argv[1:])
    this_command = this_argdict[CLI_KEY_CMD]

    if CLI_KEY_IFILE in this_argdict:
        this_file = this_argdict[CLI_KEY_IFILE]
        if not is_valid_file(this_argdict[CLI_KEY_IFILE]):
            print("Bad file: ", this_file)
            sys.exit(1)

    if this_command == CLI_KEY_COUNT:
        this_file = this_argdict[CLI_KEY_IFILE]
        UT.count_items(this_file, sys.stdout)

    if this_command == CLI_KEY_SUMMARIZE:
        this_file = this_argdict[CLI_KEY_IFILE]
        UT.summarize_items(this_file, sys.stdout)

    if this_command == CLI_KEY_EXTRACT:
        in_filename  = this_argdict[CLI_KEY_IFILE]
        out_filename = this_argdict[CLI_KEY_OFILE]
        try:
            startstr    = this_argdict[CLI_KEY_START]
            endstr      = this_argdict[CLI_KEY_END]
            startno     = int(startstr)
            endno       = int(endstr)  + 1
        except ValueError:
            sys.stderr.write(f"Bad range: {startstr} - {endstr} ")
            sys.exit(1)

        UT.pretty_print_xml_range(in_filename, out_filename, range(startno,endno))

    if this_command == CLI_KEY_MERGE:
        out_filename = this_argdict[CLI_KEY_OFILE]
        in_filenames = this_argdict[CLI_KEY_IFILES]
        UT.merge_files(in_filenames, out_filename)


if __name__ == "__main__":
    main()
