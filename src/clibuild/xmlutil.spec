# -*- mode: python -*-

block_cipher = None


a = Analysis([r'..\xmlutil.py'],
             pathex=[r'C:\newproj\xml_helper'],
             #binaries=[],
             #datas=[],
             hiddenimports=['configparser'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[]
             #win_no_prefer_redirects=False,
             #win_private_assemblies=False
             #cipher=block_cipher
			 )
a.datas = [i for i in a.datas if i[0].find('Include') < 0]
pyz = PYZ(a.pure, 
			#a.zipped_data,
             #cipher=block_cipher
			 )
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='xmlutil.exe',
          debug=False,
          strip=False,
          upx=True,
          console=True , version='xmlutil-version.txt', icon='windows_icon.ico')
