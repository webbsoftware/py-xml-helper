import xml.sax
from typing import *

from lxml import etree as ET

from .xml_handler import *
##import xml_handler


class LargeXMLProcessor():
    handler     : LargeXMLContentHandler

    def __init__(self):
        self.output_fh      = None
        self.output_started = False
        self.master_tag     = ""
        self.parser         = xml.sax.make_parser()
        self.handler        = LargeXMLContentHandler()
        self.parser.setContentHandler(self.handler)
        self.split_max      = 0
        self.split_count    = 0

    def _output_handler(self, master_tag:str, element:ET.Element):
        if not self.output_started:
            self.master_tag = master_tag
            self._output_header()
            self.output_started = True

        self.output_tree(element)

    def output_tree(self, element:ET.Element):
        INDENT = "  "
        xml_string = ET.tostring(element, pretty_print=True, xml_declaration=False).decode('utf-8')
        xml_lines = xml_string.splitlines()
        for line in xml_lines:
            self.output_fh.write(f"{INDENT}{line}\n")

    def _output_header(self):
        if self.output_fh:
            self.output_fh.write(f"<{self.master_tag}>\n")

    def _output_footer(self):
        if self.output_fh and self.output_started:
            self.output_fh.write(f"</{self.master_tag}>\n")

    def add_post_handler(self, handler_function):
        self.handler.add_post_handler(handler_function)
        return True

    def load_file(self, filename:str):
        self.parser.parse(filename)
        self._output_footer()
        return self.handler.get_first_items_counter()

    def load_files(self, filenames:List[str]):
        for filename in filenames:
            self.parser.parse(filename)
        self._output_footer()
        return self.handler.get_first_items_counter()

    def add_output(self, file_handle):
        self.output_fh = file_handle
        self.handler.add_output_handler(self._output_handler)

    def add_rangefilter(self, rangefilter:range):
        self.handler.add_rangefilter(rangefilter)

    def split_content_handler(self, master_tag:str, element:ET.Element):
        pass

    def split_file(self, filename:str, items_per_file:int):
        self.split_max = items_per_file
        self.handler.add_post_handler(self.split_content_handler)
        #self.parser.parse(filename)

    def get_total_dict(self):
        return self.handler.get_total_dict()