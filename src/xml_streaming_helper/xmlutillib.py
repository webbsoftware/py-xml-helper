import sys
from typing import *

from lxml import etree as ET

from .xml_handler   import *
from .xml_processor import *


def output_xml_to_file(input_filename, output_filename, xmltool):
    with open(output_filename, "w") as fh:
        xmltool.add_output(fh)
        xmltool.load_file(input_filename)


def pretty_print_xml(input_filename:str, output_filename:str):
    xmltool = LargeXMLProcessor()
    output_xml_to_file(input_filename, output_filename, xmltool)


def pretty_print_xml_range(input_filename:str, output_filename:str, rangefilter:range):
    xmltool = LargeXMLProcessor()
    xmltool.add_rangefilter(rangefilter)
    output_xml_to_file(input_filename, output_filename, xmltool)


def split_file(input_filename:str, items_per_file:int):
    xmltool = LargeXMLProcessor()
    xmltool.split_file(input_filename, items_per_file)


def count_items(input_filename:str, file_handle):
    xmltool = LargeXMLProcessor()
    count = xmltool.load_file(input_filename)
    file_handle.write(f"Total items: {count}\n")


def summarize_items(input_filename:str, file_handle):
    xmltool = LargeXMLProcessor()
    count = xmltool.load_file(input_filename)
    file_handle.write("\n")
    file_handle.write(f"Total items: {count}\n")
    total_dict = xmltool.get_total_dict()
    for key in total_dict.keys():
        print(f"  {key} : {total_dict[key]}")


def merge_files(input_files:List[str], output_filename:str):
    xmltool = LargeXMLProcessor()
    with open(output_filename, "w") as fh:
        xmltool.add_output(fh)
        xmltool.load_files(input_files)
