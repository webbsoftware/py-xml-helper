import xml.sax

from lxml import etree as ET


class LargeXMLContentHandler(xml.sax.ContentHandler):
    #current_tag : str

    def __init__(self):
        self._clear_settings()
        self.post_level1_handler    = None
        self.output_handler         = None
        self.rangefilter            = None
        self.first_items_count      = 0
        self.total_count            = 0
        self.total_dict             = {}

    def _increment_total_counter(self, tag_name:str):
        self.total_count += 1
        if tag_name not in self.total_dict:
            self.total_dict[tag_name] = 0
        self.total_dict[tag_name] += 1

    def _increment_item_counter(self):
        self.first_items_count += 1

    def _clear_settings(self):
        self.indent_space_count     = 2
        self.master_tag             = ""
        self.indent_level           = 0
        self.mini_doc               = None

    def _increment_indent(self):
        self.indent_level += 1

    def _decrement_indent(self):
        self.indent_level -= 1

    def _is_top_level(self):
        return self.indent_level == 1

    def _get_current_indent_string(self) -> str:
        return " " * (self.indent_level * self.indent_space_count)

    def get_first_items_counter(self):
        return self.first_items_count

    def get_total_dict(self):
        return self.total_dict

    def startDocument(self):
        self._clear_settings()
        #print("\n---start---")

    def endDocument(self):
        pass
        #print("---end--- " + self.master_tag)

    def _create_new_tree(self):
        self.mini_doc = ET.Element("top")
        self.current_node = self.mini_doc

    def _add_new_element(self, tag_name, attributes):
        element = ET.SubElement(self.current_node, tag_name)
        for key in attributes:
            element.set(key, attributes[key])
        self.current_node = element

    def startElement(self, tag, attributes):
        if not self.master_tag:
            self.master_tag = tag
        else:
            self._increment_indent()

        if self._is_top_level():
            self._create_new_tree()

        if tag != self.master_tag:
            self._add_new_element(tag, attributes._attrs)

        #print(self._get_current_indent_string() + f"<{tag}>" + " : " + str(attributes._attrs))
        self.current_tag = tag

    def run_handlers(self, element:ET.Element):
        self._increment_total_counter(element.tag)
        if self.rangefilter:
            if not ((self.total_count) in self.rangefilter):
                return

        self._increment_item_counter()
        if self.post_level1_handler:
            self.post_level1_handler(self.master_tag, element)
        if self.output_handler:
            self.output_handler(self.master_tag, element)

    def endElement(self, tag):
        if self._is_top_level():
            for element in self.mini_doc.iterchildren():
                self.run_handlers(element)
                #print(ET.tostring(element, pretty_print=True, xml_declaration=False).decode('utf-8'))
                break
        else:
            self.current_node = self.current_node.getparent()
        if (tag != self.master_tag):
            self._decrement_indent()
        self.current_tag = ""

    def _add_text(self, text_info):
        text_info = text_info.rstrip()
        if text_info:
            self.current_node.text = text_info

    def characters(self, content):
        info = content.rstrip()
        self._add_text(content)

    def add_post_handler(self, handler_function):
        self.post_level1_handler = handler_function

    def add_output_handler(self, handler_function):
        self.output_handler = handler_function

    def add_rangefilter(self, rangefilter:range):
        self.rangefilter = rangefilter